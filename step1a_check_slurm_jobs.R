# library load 
library(pacman)
p_load(tidyverse, argparse, glue, data.table)
# argument parser
parser = ArgumentParser(description = "Check job completion and resubmit bash jobs for failed jobs for the UCUM Unit Transformation step")
parser$add_argument("-o", "--output", help = "Output file path", required = TRUE)
parser$add_argument("-JI", "--job_id", help = "previous submissions job id", default=TRUE)
parser$add_argument("-A", "--slurm_account", help = "slurm account for job submission", required = TRUE)
parser$add_argument("-M", "--slurm_mem", help = "slurm memory for job submission", required = TRUE)
parser$add_argument("-T", "--slurm_time", help = "slurm time for job submission", required = TRUE)
parser$add_argument("-S", "--slurm_files_dir", help = "slurm files directory", default=TRUE)
parser$add_argument("-s", "--jobs_start_index", help = "start index for slurm jobs", default=1)
parser$add_argument("-e", "--jobs_end_index", help = "end index for slurm jobs", default='all')

args <- parser$parse_args()

log.path<-"step1a_transform_ucum_units_check_slurm_log.txt"
log.conn<-file(log.path, 'w')
write(paste0("This script was run at ", Sys.time()), file=log.conn, append=FALSE)

#check for slurm files directory 
slurm.files.dir=args$slurm_files_dir
if(slurm.files.dir==TRUE){
  slurm.files.dir=file.path(args$output, 'slurm_files')
}

if(!dir.exists(slurm.files.dir)) {
  msg<-paste0("The slurm output directory at ", slurm.files.dir, " doesn't exist.  Check and make sure the --slurm_files_dir or --output is correct and/or that you have already run the step4 script.")
  print(msg)
  write(msg, file=log.conn, append=TRUE)
  stop()
}
slurm.output.dir=file.path(slurm.files.dir, 'job.output')
slurm.job.array.id<-args$job_id

print(paste0("checking ", slurm.output.dir, " for files that fit name pattern substep_process_lab__", slurm.job.array.id,"_[0-9]*.out"))

this.job.array.slurm.output.files<-dir(slurm.output.dir, pattern=paste0("substep_process_lab__", slurm.job.array.id,"_[0-9]*.out"))

print(this.job.array.slurm.output.files)

if (length(this.job.array.slurm.output.files)==0)
{
  msg<-paste0("There were no slurm job output files found at ", slurm.output.dir, ".  Check and make sure the --output or --slurm_files_dir is correct and/or that you have already completed the scripts for the previous step (step1a).")
  print(msg)
  write(msg, file=log.conn, append=TRUE)
  stop()
}

completed.batches<-c()
incompleted.batches<-c()

for (o in 1:length(this.job.array.slurm.output.files))
{
  print(o)
  this.out.path<-file.path(slurm.output.dir,this.job.array.slurm.output.files[o])
  
  #get the batch id
  this.batch.id<-as.numeric(gsub(".*_([0-9]+).out.*", "\\1", this.out.path))
  cmd<-paste0("tail -n 1 ", this.out.path)
  cmd1<-paste0("grep 'ERROR' ", this.out.path)
  res<-system(cmd, intern = TRUE)
  suppressWarnings(res1<-system(cmd1, intern=TRUE))
  result=ifelse(res=='All cmds completed successfully' & rlang::is_empty(res1), 'PASS', 'FAIL')
  if (result=='PASS')
  {
    completed.batches<-append(completed.batches, this.batch.id)
  }else
  {
    incompleted.batches<-append(incompleted.batches,this.batch.id)
  }
}

msg<-paste0("Completed ", length(completed.batches), " batches out of ", length(this.job.array.slurm.output.files), " for this job array.  ", length(incompleted.batches), " batches found that were not run to completion.  Resubmitting those batches using the user-specified slurm memory and time parameters.")
print(msg)
write(msg, file=log.conn, append=TRUE)


if (length(incompleted.batches) == 0)
{
  msg<-paste0("There are no remaining jobs from the ", slurm.job.array.id, " job array that need to be completed.  Done")
  print(msg)
  write(msg, file=log.conn, append=TRUE)
  stop()  

}

jobs.account=args$slurm_account
jobs.mem=args$slurm_mem
jobs.time=args$slurm_time

sbatch.param.account<-""
if (!is.na(jobs.account))
{
  if (nchar(jobs.account) > 0)
    sbatch.param.account<-paste0("#SBATCH --account=", jobs.account)
}

slurm.output.path<-file.path(slurm.output.dir, "substep_process_lab_")

slurmcmd<-paste0("#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem=", jobs.mem ,"
#SBATCH --time=", jobs.time,"
#SBATCH --output=", slurm.output.path,"_%A_%a.out
#SBATCH --job-name=\"QualLab_process_labs\"
", sbatch.param.account, "
#SBATCH --array=", paste(incompleted.batches, collapse=","),"

", "FILES=($(ls ", slurm.files.dir, "/*.sh))",
"\n", 
file.path("${FILES[$SLURM_ARRAY_TASK_ID]}"))

new.slurm.path<-file.path(slurm.files.dir, paste0("call.process.labs.jobs.slurm", ".jobs_job.array.", slurm.job.array.id,".reruns.slurm"))
write(slurmcmd, file=new.slurm.path)

cmd<-paste0("sbatch ", new.slurm.path)
system(cmd)

msg<-paste0("Created ", new.slurm.path, " batch file in ", slurm.files.dir, " and submitted batch job.")
print(msg)
write(msg, file=log.conn, append=FALSE)

close(log.conn)