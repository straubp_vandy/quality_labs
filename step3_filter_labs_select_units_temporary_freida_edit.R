#This script will take the filter parameters specficied by the user and find all labs/units that meet those filters.  Flat files for the filtered labs/units will be generated.

library("data.table")

source("shared_functions.R")






sqlite.path<-NA #--sqlite-db-path
#flat.files.delim<-NA #--flat-files-delim
flat.files.dir<-NA #--flat-files-dir
filtered.flat.files.dir<-NA #--filtered-flat-files-dir
filt.min.numeric<-NA #--min-numeric-vals
filt.min.numeric.per.unit<-NA #--min-numeric-vals-per-unit
filt.min.numeric.per.unit.per.sample<-NA #--min-numeric-vals-per-unit-sample
filt.min.sample<-NA #--min-num-sample

filt.min.units.in.lab<-NA #--min-units-in-lab
filt.max.units.in.lab<-NA #--max-units-in-lab
filt.min.percent.units.in.lab<-NA #--min-percent-units-in-lab
filt.max.percent.units.in.lab<-NA #--max-percent-units-in-lab
filt.unit.regex<-NA #--unit-name-regex
filt.lab.regex<-NA #--lab-name-regex
unmapped.only<-FALSE #--unmapped-only
save.selection.in.sqlite.db<-FALSE #--save-selection-in-sqlite
overwrite.existing<-FALSE #--overwrite-existing



args<-commandArgs(trailingOnly = TRUE)

log.path<-"step3_filter_labs_select_units_log.txt"
log.conn<-file(log.path, 'w')
write(paste0("This script was run at ", Sys.time()), file=log.conn, append=FALSE)





args.error<-c()
unrecognized.args<-c()
numeric.args<-c("--min-numeric-vals", "--min-numeric-vals-per-unit", "--min-numeric-vals-per-unit-sample", "--min-num-sample", "--min-units-in-lab", "--max-units-in-lab", "--min-percent-units-in-lab", "--max-percent-units-in-lab")
bool.args<-c("--unmapped-only", "--save-selection-in-sqlite", "--overwrite-existing")
#required.args<-c("--sqlite-db-path", "--flat-files-delim", "--flat-files-dir", "--filtered-flat-files-dir")
required.args<-c("--sqlite-db-path", "--flat-files-dir", "--filtered-flat-files-dir")

print(args)
if (length(args) > 0)
{
  
  
  print("Checking for required parameters")
  write(paste0("Checking for required parameters"), file=log.conn, append=TRUE)
  
  
  #first, check and make sure the required arguments are assigned
  missing.required<-setdiff(required.args, args)
  print(paste0("missing required args = ", missing.required))
  if (length(missing.required) > 0)
  {
    for (m in 1:length(missing.required))
    {
      this.missing<-missing.required[m]
      this.error<-paste0("The ", this.missing, " argument is required.")
      args.error<-append(args.error, this.error)
    }
  }
  
  #now, combine the args in key/value pairs
  arg.keyval<-c()
  a <- 1
  while (a <= length(args))
  {
    this.key<-args[a]
    this.val<-""
    if (a <= (length(args) - 1))
    {
      this.val<-args[a+1]
    }
    arg.keyval<-append(arg.keyval, paste0(this.key, " ", this.val))
    a<-a+2
  }
  arg.keyval
  
  for (a in 1:length(arg.keyval))
  {
    if (1==2)
    {  
      this.keyval<-unlist(strsplit(arg.keyval[a], " "))
      this.arg<-trim(this.keyval[1])
      this.val<-trim(this.keyval[2])
      
      #if the parameter value was an empty string, it would get set to an NA here so let's change it back to an empty string
      if (is.na(this.val))
        this.val<-""
    }
    
    this.keyval<-unlist(regmatches(arg.keyval[a], regexpr(" ", arg.keyval[a]), invert = TRUE))
    
    
    this.arg<-trim(this.keyval[1])
    if (length(this.keyval) > 1)
    {
      this.val<-this.keyval[2]
    }else
    {
      this.val<-""
    }
    
    
    
    is.arg.numeric<-this.arg %in% numeric.args
    is.arg.bool<-this.arg %in% bool.args
    this.vals<-parseArgs(this.arg, this.val, is.arg.numeric, is.arg.bool)
    
    #check if there was an error
    if (nchar(this.vals$error) > 0)
    {
      args.error<-append(args.error, this.vals$error)
      
    }else
    {
      if (this.arg=="--min-numeric-vals")
      {
        filt.min.numeric<-this.vals$arg_val
      }else if (this.arg=="--min-numeric-vals-per-unit")
      {
        filt.min.numeric.per.unit<-this.vals$arg_val
      }else if (this.arg=="--min-numeric-vals-per-unit-sample")
      {
        filt.min.numeric.per.unit.per.sample<-this.vals$arg_val
      }else if (this.arg=="--min-num-sample")
      {
        filt.min.sample<-this.vals$arg_val
      }else if (this.arg=="--min-units-in-lab")
      {
        filt.min.units.in.lab<-this.vals$arg_val
      }else if (this.arg=="--max-units-in-lab")
      {
        filt.max.units.in.lab<-this.vals$arg_val
      }else if (this.arg=="--lab-name-regex")
      {
        filt.lab.regex<-this.vals$arg_val
      }else if (this.arg=="--unit-name-regex")
      {
        filt.unit.regex<-this.vals$arg_val
      }else if (this.arg=="--min-percent-units-in-lab")
      {
        filt.min.percent.units.in.lab<-this.vals$arg_val
      }else if (this.arg=="--max-percent-units-in-lab")
      {
        filt.max.percent.units.in.lab<-this.vals$arg_val
      }else if (this.arg=="--sqlite-db-path")
      {
        sqlite.path<-this.vals$arg_val
        
      }else if (this.arg=="--flat-files-dir")
      {
        flat.files.dir<-this.vals$arg_val
      }else if (this.arg=="--filtered-flat-files-dir")
      {
        filtered.flat.files.dir<-this.vals$arg_val
      }else if (this.arg=="--unmapped-only")
      {
        unmapped.only<-this.vals$arg_val
      }else if (this.arg=="--save-selection-in-sqlite")
      {
        save.selection.in.sqlite.db<-this.vals$arg_val
      }else if (this.arg=="--overwrite-existing")
      {
        
        overwrite.existing<-this.vals$arg_val
      }else
      {
        args.error<-append(args.error, paste0("The ", this.arg, " parameter is not a recognized parameter."))
      }
      
    }
    
  }
}else
{
  args.error<-append(args.error, "You did not provide any parameters.")
  
}

print(paste0("length args.error=", length(args.error), "=", args.error))
if (length(args.error) > 0)
{
  print("There were errors with your input parameters:")
  write(paste0("There were errors with your input parameters:"), file=log.conn, append=TRUE)
  
  
  
  
  
  for (a in 1:length(args.error))
  {
    print("
")
    print(args.error[a])
    
    write(paste0("
", args.error[a]), file=log.conn, append=TRUE)
    
    
  }
  
  param.instructions<-"

The available parameters are as follows:
                  
--flat-files-dir = REQUIRED.  The path to the directory where the lab flat files were generated by the step1_create_sqlite_db.R script.
--sqlite-db-path = REQUIRED. The path to the sqlite database path.  This should be the same path you selected when running the step2_create_sqlite_db.R script.
--filtered-flat-files-dir = REQUIRED. The directory where the selected labs/units flat will be created.

The following are the different types of filters you can impose.  You can select any combination of filters:
--min-numeric-vals = NOT REQUIRED.  The minimum # of numeric values (across all units in lab).
--min-numeric-vals-per-unit = NOT REQUIRED.  The minimum # of numeric values for a given unit for a given lab (across all samples for that lab).  Default value of 1 if not set by user. 
--min-numeric-vals-per-unit-sample = NOT REQUIRED.  The minimum # of numeric values for a given unit for a given sample for a given lab.  This value should be one of the values in the --sample-per-lab-per-unit-num-numeric-values-thresholds parameter from the step2_create_sqlite_db.R script (defaults to 1,2,3,4,5,10,30,50,100, 250, 500, 750, or 1000).  This parameter should be used with the --min-num-sample parameter.  If --min-num-sample is not specified it defaults 1.  Default value of 1 for min-numeric-vals-per-unit-sample if not set by user. 
--min-num-sample = NOT REQUIRED.  The minimum # of samples for a given lab/unit pair who pass the minimum # of numeric values threshold set by the --min-numeric-vals-per-unit-sample parameter.  For example, if the user sets a --min-numeric-vals-per-unit-sample = 100 and --min-num-sample = 200, only lab/units that have at least 200 sample who each have at least 100 numeric values in that lab/unit will pass the filter.  Default value of 1 if not set by user.  
--min-units-in-lab = NOT REQUIRED.  The minimum # of different units a lab has.  Default value of 1 if not set by user. 
--max-units-in-lab = NOT REQUIRED.  The maximum # of different units a lab has.  Default value of 9999999999 if not set by user. 
--min-percent-units-in-lab = NOT REQUIRED.  The minimum % of the total numeric values for a given lab that a given unit must have.  This should be a on 0-100 scale.  Default value of 0 if not set by user.
--max-percent-units-in-lab = NOT REQUIRED.  The maximum % of the total numeric values for a given lab that a given unit must have.  This should be a on 0-100 scale.  Default value of 100 if not set by user.
--lab-name-regex = NOT REQUIRED.  A regular expression to be used for filtering based on lab names.  The regular expression match is done against the 'LAB_SHORTNAME|LAB_LONGNAME' string.
--unit-name-regex = NOT REQUIRED.  A regular expression to be used for filtering based on unit names.  
--unmapped-only = NOT REQUIRED.  A boolean parameter that, if True (T, TRUE, 1), will only consider labs/units that haven't already been exported.  The lab/units you choose to export will be stored in the sqlite database so this script will check that database first if this is set to True. 
--save-selection-in-sqlite = NOT REQUIRED.  A boolean parameter, True (T, TRUE, 1), will save the selected labs/units in the sqlite database.  If False (F, FALSE, 0), the selected labs/unit flat files will still be generated in the --filtered-flat-files-dir directory but these selections will not be saved in the sqlite database.  Default value of FALSE if not set by user.
--overwrite-existing = NOT REQUIRED.  A boolean parameter, True (T, TRUE, 1), will overwrite labs flat files if they already exist.  If False (F, FALSE, 0), existing lab flat files will not be overwritten.  Default value of FALSE if not set by user.

The lab/unit filtering is carried out in the following order:
1.  If --min-numeric-vals-per-unit-sample and --min-num-sample parameters are provided, filter the lab/units to those that have the '--min-numeric-vals-per-unit-sample' minimum # of numeric values for a given unit for a given lab for at least '--min-num-sample' # of samples.  If the --min-num-sample or --min-numeric-vals-per-unit-sample parameters are not set by the user they default to a value of 1.  
2.  If --lab-name-regex provided, filter labs to those that match the lab name regex.  This regex match is compared to the 'LAB_SHORTNAME|LAB_LONGNAME' string. 
3.  If --unit-name-regex provided, filter units to those that match the unit name regex. 
4.  If --filt-min-numeric provided, filter labs based on the overall number of numeric values (across all units for that lab).
5.  If --min-numeric-vals-per-unit provided, filter units for a given lab to those units that comprise at from labs based on the numerber of numeric values for a given unit in a given lab.
6.  If --max-numeric-vals-per-unit provided, filter units from labs based on the number of numeric values for a given unit in a given lab.
7.  If --min-units-in-lab provided, filter labs that don't have at least the specificied # of different units REMAINING AFTER ALL OF THE PREVIOUS FILTERS HAVE BEEN IMPOSED.
8.  If --max-units-in-lab provided, filter labs that don't have more than the specificied # of different units REMAINING AFTER ALL OF THE PREVIOUS FILTERS HAVE BEEN IMPOSED.


The lab/unit pairs that pass the filters will have separate flat files created in the directory specified by the --filtered-flat-files-dir parameter.

"
  
  
  cat(param.instructions)
  
  write(paste0("

",param.instructions), file=log.conn, append=TRUE)
  
  stop()  
}

msg<-paste0("
            
connecting to database...", sqlite.path)

print(msg)
write(msg, file=log.conn, append=TRUE)



dbconn<-getDBConn(sqlite.path)
if (dbIsValid(dbconn))
{
  msg<-paste0("connected to database.")
  
  print(msg)
  write(msg, file=log.conn, append=TRUE)
  
}else
{
  msg<-paste0("could not connect to database.")
  
  print(msg)
  write(msg, file=log.conn, append=TRUE)
  
  stop()
}

#this.Rdata.path<-file.path(filtered.flat.files.dir, "temp.Rdata")
#save.image(file=this.Rdata.path)


checkMapGroupDB(dbconn)


master.all.lab.counts.dat<<-getLabUnitsCountsAllUnits(dbconn)

master.all.lab.by.units.counts.dat<<-getLabUnitsCountsByUnits(dbconn)

master.min.values.per.unit.per.sample<<-0
master.min.samples<<-0

sample.subset.all.lab.counts.dat<<-master.all.lab.counts.dat
sample.subset.all.lab.by.units.counts.dat<<-master.all.lab.by.units.counts.dat


all.lab.counts.dat<<-master.all.lab.counts.dat
all.lab.by.units.counts.dat<<-master.all.lab.by.units.counts.dat

print(paste0("lab name check combo: "))
print(all.lab.by.units.counts.dat$comboname)

print(paste0("lab name check shortname: "))
print(all.lab.by.units.counts.dat$LAB_SHORTNAME)


master.all.samples.counts.by.units.thresh.dat<<-getThresholdSampleCountsByLab(dbconn)
master.all.samples.counts.by.units.thresh.values<<-unique(master.all.samples.counts.by.units.thresh.dat$num_numeric_per_unit_per_person_threshold) 





master.loaded.labs.column.names<-read.table(file.path(flat.files.dir, "column_names", "LAB_FILE_COLUMN_NAMES.txt"), header=FALSE, stringsAsFactors = FALSE)
if (nrow(master.loaded.labs.column.names) > 1)
{
  master.loaded.labs.column.names<<-unlist(master.loaded.labs.column.names[,1])
} else {
  master.loaded.labs.column.names<-unlist(master.loaded.labs.column.names[1,])
}



#because the ./create_labs_flat_files.sh file adds an extra column of the unmodified LAB_SHORTNAME at the end we need to add that to the list of column names
#print(master.loaded.labs.column.names)

#master.loaded.labs.column.names<<-append(master.loaded.labs.column.names, "LAB_SHORTNAME_UNMODIFIED")

#print(master.loaded.labs.column.names)
#stop()

master.all.loaded.lab.values.lst<<-list()
master.all.loaded.lab.shortnames<<-c()


mapped.unit.groups.dat<-getMapUnitGroupsForSelect(dbconn)

mapped.lab.groups.dat<<-getMapLabGroupsForSelect(dbconn)



mapped.or.unmapped<-"show_all"
if (unmapped.only)
{
  mapped.or.unmapped<-"unmapped_only"
}
setLabUnitCountGlobalVars(min_lab_vals=filt.min.numeric, min_unit_vals=filt.min.numeric.per.unit, min_num_units=filt.min.units.in.lab, max_num_units=filt.max.units.in.lab, min_unit_percent_vals=filt.min.percent.units.in.lab, max_unit_percent_vals=filt.max.percent.units.in.lab, show_map=mapped.or.unmapped, unit_regex=filt.unit.regex, lab_regex=filt.lab.regex, min_unit_vals_sample=filt.min.numeric.per.unit.per.sample, min_samples=filt.min.sample, conn=dbconn)

if (save.selection.in.sqlite.db)
{
  addUnitMapSoloGroups(dbconn, all.lab.by.units.counts.dat)
}

#freida edit temp 
library(stringr)
all.lab.by.units.counts.dat$comboname=str_replace(all.lab.by.units.counts.dat$comboname, '\\|NA$', paste0('|', all.lab.by.units.counts.dat$LAB_SHORTNAME))

res.labs.lst<-exportUnitMapFromFlatFileNoDB(flat_files_dir=flat.files.dir, flat_files_delim="\t", flat_files_columns=master.loaded.labs.column.names, selected_lab_unit_maps=all.lab.by.units.counts.dat, download_directory=filtered.flat.files.dir, numeric_only=TRUE, overwrite=overwrite.existing)

written.labs<-res.labs.lst$written.labs
missed.labs<-res.labs.lst$missed.labs

#let's note which labs were written selected and if they were written or not
all.lab.by.units.counts.dat$flat.file.written<-"skipped"
all.lab.by.units.counts.dat[all.lab.by.units.counts.dat$LAB_SHORTNAME %in% written.labs, "flat.file.written"]<-"written"

write.table(all.lab.by.units.counts.dat, file="quality_labs_step3_selected_labs.txt", col.names = TRUE, row.names = FALSE, quote=FALSE, sep="\t")

msg<-"A report listing which labs/unit were selected and whether or not a flat file was created is written to quality_labs_step3_selected_labs.txt"
print(msg)
write(msg, file=log.conn, append=TRUE)

write(paste0("This script was finished at ", Sys.time()), file=log.conn, append=TRUE)

close(log.conn)
print("A log file was written to step3_filter_labs_select_units_log.txt")
