const { log } = require('console');
const fs = require('fs');
const path = require('path');
const { escape } = require('querystring');

// Specify the path to the directory containing your modules
const homeDirectory = process.env.HOME || process.env.USERPROFILE;
const moduleDirectory = path.join(homeDirectory, 'node_modules');

var ucum = require(path.join(moduleDirectory, '@lhncbc/ucum-lhc'));
var utils = ucum.UcumLhcUtils.getInstance();

const readline = require('readline');

// Get command-line arguments
const args = process.argv.slice(2); // Skip the first two arguments (node and script name)

// Check if the required arguments are provided
if (args.length < 4) {
  console.error('Usage: node script.js <file_path> <unit_concept_column_number> <unit_value_column_number> <log_file_path>');
  process.exit(1);
}

// Extract arguments
const filePath = args[0];
const unitConceptColumn = parseInt(args[1], 10);
const unitValueColumn = parseInt(args[2], 10);
const logFilePath = args[3];

// Create a write stream for the log file
const logStream = fs.createWriteStream(logFilePath, { flags: 'a' });

// Map to store the count of each unit_concept
const unitConceptCount = new Map();

// Read the file line by line using a stream
const stream = readline.createInterface({
  input: fs.createReadStream(filePath),
  output: process.stdout,
  terminal: false
});

// Process each line
stream.on('line', line => {
  const columns = line.split('\t');

  // process through unit concepts to find the most common one 
  const unitConcept = columns[unitConceptColumn - 1];
  
  // Update the count for this unit concept   
  unitConceptCount.set(unitConcept, (unitConceptCount.get(unitConcept) || 0) + 1);
  
});

// When the file reading is complete
stream.on('close', () => {
  // Find the unit_concept with the maximum count
  let mostCommonUnitConcept;
  let maxCount = 0;

  unitConceptCount.forEach((count, unitConcept) => {
    if (count > maxCount) {
      mostCommonUnitConcept = unitConcept;
      maxCount = count;
    }
  });

  // Write the most common unit_concept to the log file
  logStream.write(`Most common unit_concept: ${mostCommonUnitConcept}\n`);

  // Check if the mostCommonUnitConcept is a UCUM unit type
  var ucum_check = utils.validateUnitString(mostCommonUnitConcept);
  if (ucum_check['status'] === 'valid') {
    logStream.write('Most common unit is a UCUM valid string: \n');
    logStream.write(JSON.stringify(ucum_check['unit']));
  } else {
    logStream.write('WARNING: Most common unit is NOT a UCUM valid string... no conversion will be performed');
    logStream.write(JSON.stringify(ucum_check['msg']));
  }

  logStream.end();

  // Read the file again to convert unit_values
  const convertStream = readline.createInterface({
    input: fs.createReadStream(filePath),
    output: process.stdout,
    terminal: false
  });

  // Process each line and add new columns with converted values and the majority unit_concept
  convertStream.on('line', line => {
    const columns = line.split('\t');
    const logStream = fs.createWriteStream(logFilePath, { flags: 'a' });
    // Get the original unit_concept and unit_value
    const unitConcept = columns[unitConceptColumn - 1];
    const unitValue = columns[unitValueColumn - 1];

    // Initialize convertedValue and convertedConcept
    let convertedValue = '';
    let convertedConcept = '';
    // Check if mostCommonUnitConcept is blank or 'NA'
    if (ucum_check['status'] === 'valid') {
      // check if unitConcept not = to the mostCommon unit concept
      if (unitConcept !== mostCommonUnitConcept) {
        returnObj = utils.convertUnitTo(unitConcept, parseFloat(unitValue), mostCommonUnitConcept);
        convertedValue = returnObj['toVal'];
        convertedConcept = mostCommonUnitConcept;
        if (returnObj['status'] !== 'succeeded') {
          logStream.write('\n WARNING: conversion failure for unitConcept: ${unitConcept} \n');
          logStream.write(JSON.stringify(returnObj['msg']));
          convertedConcept = 'null'
        }
      } else {
        // If unitConcept is equal to mostCommonUnitconcept
        convertedValue = unitValue;
        convertedConcept = unitConcept;
      }
    } else {
      convertedValue = unitValue;
      convertedConcept = unitConcept;
    }

    // Add new columns with the converted value and concept, and the majority unit_concept
    const newLine = line + `\t${convertedValue}\t${convertedConcept}\t${mostCommonUnitConcept || ''}`;
    console.log(newLine);
     // Close the log stream after processing is complete
    logStream.end();
  });
});

// Function to handle writing to the log stream
function writeToLogStream(message) {
  if (!logStream.writableEnded) {
    logStream.write(message);
  }
}

// Handle uncaught exceptions to ensure proper closing of the log stream
process.on('uncaughtException', err => {
  console.error('Uncaught exception:', err);
  writeToLogStream(`Uncaught exception: ${err}\n`);
  logStream.end();
  process.exit(1);
});

// Handle unhandled promise rejections to ensure proper closing of the log stream
process.on('unhandledRejection', (reason, promise) => {
  console.error('Unhandled promise rejection:', reason);
  writeToLogStream(`Unhandled promise rejection: ${reason}\n`);
  logStream.end();
  process.exit(1);
});