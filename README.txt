There are 5 "steps" in the QC pipeline that are run with the R scripts.  You can get a full list of the available parameters by running the script (like "Rscript step1_create_flate_files.R" without any arguments).

Here's an overview of how to run each step:

1.  step1_create_flat_files.R: Parses the input lab flat file, and script creates a bash script (create_labs_flat_files.sh) that will split the flat file into separate flat files for each lab.  
The bash script runs an awk (gawk) command that will read line by line through the lab input file and write the line to the separate lab file based on the LAB_SHORTNAME column.  

For this step you'll need your labs flat file.  You'll need to map all of the required columns to the Quality Labs required columns using the following parameters
--input-col-person-id, --input-col-lab-date, --input-col-lab-shortname,
--input-col-lab-longname, --input-col-lab-value, and --input-col-lab-unit.  

You also need to specificy the input file delimiter (--input-labs-flat-file-delim) and destination directory (--labs-flat-files-creation-dir) for the partitioned lab set that gets generated by create_labs_flat_files.sh.

You probably want to run it like:

Rscript step1_create_flat_files.R --input-labs-flat-file-path /path/to/your/labs.txt.gz --labs-flat-files-creation-dir /path/to/QL/mylabsproject/input_labs_partitioned/ --input-labs-flat-file-delim '\t' --system-unzip-command zcat --input-labs-flat-file-is-zipped TRUE --input-col-person-id patient_ID --input-col-lab-value value_numeric --input-col-lab-date clin_date --input-col-lab-shortname lab_abbrev --input-col-lab-longname lab_name --input-col-lab-unit unit

Then look and see if the create_labs_flat_files.sh file was created in your run directory.  If so, run that bash script. It might take a while if you have a huge input file.  But any size input file should work since it's read line-by-line.  

1A. Once you've run the create_labs_flat_files.sh script to completion, and the set of separate lab flat files are created, you have the option to do Step 1A, which will transform UCUM units using the UCUM unit vocabulary. This is run in a couple of steps: 

a. First, run Rscript step1a_transform_ucum_units. For this, you need a concept map file that maps concept_ids of labs to their UCUM unit measurement. I obtained this from the concept table of the SD by filtering to only those rows where vocabulary_id==UCUM. 
You'll also need the javascript ucum-lhc library. You can provide a path, and if the script doesn't find the ucum-lhc java library, it will warn you and attempt to install using npm install @lhncbc/ucum-lhc@3.1.0 --prefix DIR_YOU_PROVIDE 
To run the javascript libraries, you need nodejs/.16.15.1. The script is written assuming a slurm submission system and loads the nodejs library available on Accre, but this will be different and needs to be modified for other systems 
This Rscript will launch parallel slurm jobs for all of the labs in the provided directory, first joining the per-lab flat file to the concept map of UCUM units, then using the lhc UCUM javascript library to select the majority UCUM unit and convert all other UCUM units into that unit basis. 
In the log folder, it will provide per-lab log files that will contain information on the most common unit concept and conversions. For labs for which the most common unit is NOT a UCUM unit, no conversions will take place, and subsequent unit selection will take place as previously implemented in Quality Labs and as described in below steps. 
This step will call the subprocesses substep1a_join.R and substep1a_ucum_convert.js 

b. Then run step1a_check_slurm_jobs.R, increasing the slurm_mem and slurm_time options as needed to complete the jobs 

c. finally, you can run --step1b_compare_distributions.R if you want to see how the distribution of lab values differed across the different initial UCUM units 

2.  Once you've run the create_labs_flat_files.sh script to completion (and optionally 1A), and the set of separate lab flat files are created, move onto the step2 script, step2_create_sqlite_db.R: This script creates the sqlite database that stores various lab summary stats.  You just need to specify the path to the sqlite database that will be created with the --sqlite-db-path.  And then also specify the --labs-flat-files-creation-dir which is typically the same value used for this parameter in step1.  

You'll want to run the script as follows:

Rscript step2_create_sqlite_db.R --sqlite-db-path /path/to/QL/mylabsproject/sqlite/mylabsproject_labssummarystats.sqlite --labs-flat-files-creation-dir /path/to/QL/mylabsproject/input_labs_partitioned/

3.  For Step 3, using the step3_filter_labs_select_units.R script, you'll filter your labs AND units down to the set of labs/units that will get QCed summary sets generated in the next step.  This is the step where you impose filters like the minimum number of numeric values (e.g. a lab must have at least 1000 numeric values), or a minumum number of different PERSON_IDs.  You can select any combination of filters.  Note that there is an R Shiny App available that allows for the examination of individual labs and the relative number and distribution of the values for different units, which can sometimes be crucial for understanding the labs with large amounts of data in different units.

The --flat-files-dir parameter should be the same as the --labs-flat-files-creation-dir in steps 1 and 2.  
The --sqlite-db-path parameter should be the same as step 2.
The --filtered-flat-files-dir is where the filtered set of labs will be created.  This filtered set of lab files will have non-numeric entries removed.    

There's are eight different numeric filters you can impose:

--min-numeric-vals = The minimum # of numeric values (across all units in lab).
--min-numeric-vals-per-unit = The minimum # of numeric values for a given unit for a given lab (across all samples for that lab).  Default value of 1 if not set by user. 
--min-numeric-vals-per-unit-sample = The minimum # of numeric values for a given unit for a given sample for a given lab.  This parameter should be used with the --min-num-sample parameter.  If --min-num-sample is not specified it defaults 1.  Default value of 1 for min-numeric-vals-per-unit-sample if not set by user. 
--min-num-sample = The minimum # of samples for a given lab/unit pair who pass the minimum # of numeric values threshold set by the --min-numeric-vals-per-unit-sample parameter.  For example, if the user sets a --min-numeric-vals-per-unit-sample = 100 and --min-num-sample = 200, only lab/units that have at least 200 sample who each have at least 100 numeric values in that lab/unit will pass the filter.  Default value of 1 if not set by user.  
--min-units-in-lab = The minimum # of different units a lab has.  Default value of 1 if not set by user. 
--max-units-in-lab = The maximum # of different units a lab has.  Default value of 9999999999 if not set by user. 
--min-percent-units-in-lab = The minimum % of the total numeric values for a given lab that a given unit must have.  This should be a on 0-100 scale.  Default value of 0 if not set by user.
--max-percent-units-in-lab = The maximum % of the total numeric values for a given lab that a given unit must have.  This should be a on 0-100 scale.  Default value of 100 if not set by user.

You can also filter based on lab name text searches with --lab-name-regex, which is useful if you just have one or two labs of interest.  

In general, there's a few filters you'll probably generally want to apply.  --min-numeric-vals and --min-numeric-vals_per-unit are probably always useful.  Along with --min-num-sample filter to throw out the labs with under (100?  10?) different people.  

The --min-numeric-vals-per-unit-sample filter lets you create relatively well-powered sets (like everyone has to have at least 10 values in the selected unit).

The --min-percent-units-in-lab should probably always be used, and probably at a level above 50.  That's because all of the selected data that is used to generate the summary data in the following steps are pooled together and all data is treated as coming from the same unit.  So if you select a value of at least 50 percent for this parameter you will be selecting the single majority unit, if one exists.  In BioVU we especially with the large labs like two or three different units that are clearly coming from different clinics or something and have very large sample sizes but no one unit exceeds 50.  Those are the cases to be avoided.

The --max-percent-units-in-lab can be used with --min-percent-units-in-lab and a high --min-numeric-vals to find labs in a search range in the 40-50 percent to look for labs that have units that don't exceed 50 percent but are still big sets.  You can output the sets from this kind of filter to a different output folder from the '>50' filtered set and process it separately downstream.  

If you want play it safe, setting a --min-numeric-vals to 1000 and --min-percent-units-in-lab at 51 is a decent starting point.
 
If you wanted to impose a filter of at least 1000 numeric values for the lab unit with the majority of the data and at least 100 different people, you would run this script like:
Rscript step3_filter_labs_select_units.R --flat-files-dir /path/to/QL/mylabsproject/input_labs_partitioned/ --sqlite-db-path /path/to/QL/mylabsproject/sqlite/mylabsproject_labssummarystats.sqlite --filtered-flat-files-dir /path/to/QL/mylabsproject/labs_filt1/ --min-numeric-vals-per-unit 1000 --min-percent-units-in-lab 50.1 --min-num-sample 100

4.  For Step 4, you will two options for completing this step.  

4.a  The easy, but slower option:  The easiest, but likely slowest option, is to run the step4_create_summary_files.R.  The step4_create_summary_files.R script generates the QCed summary file sets for each of the labs selected in the previous step, but only one lab at a time.  This script might be too slow if you have large numbers of labs to process and/or your labs contain large amounts of data.  

For the step4_create_summary_files.R script, you will use the same --filtered-flat-files-dir parameter you set when running step 3.

This is the first step where you need a demographics file (--demo-file-path).  It should have columns for each person corresponding to the following:  PERSON_ID, DOB, SEX, RACE.  You will need to map those column names with --demo-col-person-id, --demo-col-person-dob, --demo-col-person-sex, --demo-col-person-race, and --demo-col-person-id parameters.  

You also need to specify the date format for both the LAB_DATE values (--lab-date-format) and the the DOB dates in the demo file (--demo-file-dob-date-format).  If it's 2000/12/31, that would be like "%Y/%m/%d".  One of the date string patterns from here should work: https://www.ibm.com/support/knowledgecenter/en/SSEPCD_10.1.0/com.ibm.ondemand.mp.doc/arsa0257.htm

The --demo-file-quote-char should probably be set to '"', that's a double-quote in a single quotes.  And the --demo-file-comment-char should probably be "" (empty quote)

You'll probably want to run this script something like:

Rscript step4_create_summary_grids.R --filtered-flat-files-dir /path/to/QL/mylabsproject/labs_filt1/ --lab-summary-dir /path/to/QL/mylabsproject/labs_filt1_summaries/ --lab-date-format "%Y/%m/%d" --demo-file-path /path/to/demo.file.txt --demo-file-delim , --demo-file-quote-char '"' --demo-file-comment-char "" --demo-file-dob-date-format "%Y-%m-%d" --demo-col-person-id PATIENT_ID --demo-col-person-dob PATIENT_DOB --demo-col-person-race PATIENT_RACE --demo-col-person-sex PATIENT_GENDER

4.b  Processing labs in parallel with Slurm:  If processing the labs one lab at a time is too slow, and you have access to a computing cluster with a slurm job manager, you can process the labs in parallel using Slurms job arrays with the step4_create_summary_grids_use_slurm.R (creates jobs files and submits initial job array) and step4_create_summary_grids_use_slurm_job_check.R (looks for failed jobs and resubmits them with new memory and/or time job parameters).  

I.  The step4_create_summary_grids_use_slurm.R script uses the same parameters as step4_create_summary_grids.R but with additional parameters corresponding to the slurm jobs that will be submitted.  Each lab is submitted as its own job, which are all submitted as a single job array (i.e. if you have 1005 labs, there were be 1005 separate .sh files created for each lab's job and they will be submitted together as a slurm job array batch submission to your cluster:

The --slurm-job-mem and --slurm-job-time parameters are both required.  Use full descriptions (like "5GB" and "24:00:00" not "5" and "24").

Keep in mind that you do NOT need to set memory or time values that will meet the needs of your largest lab.  You will be able to iteratively reattempt failed large labs that run out of time or memory with the step4_create_summary_grids_use_slurm_job_check.R script if necessary.  The --slurm-job-account parameter is available if you have a specific cluster user account you want the jobs run under.  

If you want to conduct a test run with, for example, 10 labs, you can use the --slurm-job-subset-start-index and --slurm-job-subset-end-index parameters.  The index of each lab corresponds to its alphanumeric order of all of the labs found in the --filtered-flat-files-dir directory.

So if you want to do a trial run of the first 10 labs (by alphabetical order in the --filtered-flat-files-dir directory) for 4 hours a job using slurm, you would set it up as follows:

Rscript step4_create_summary_grids_use_slurm.R --filtered-flat-files-dir /path/to/QL/mylabsproject/labs_filt1/ --lab-summary-dir /path/to/QL/mylabsproject/labs_filt1_summaries/ --lab-date-format "%Y/%m/%d" --demo-file-path /path/to/demo.file.txt --demo-file-delim , --demo-file-quote-char '"' --demo-file-comment-char "" --demo-file-dob-date-format "%Y-%m-%d" --demo-col-person-id PATIENT_ID --demo-col-person-dob PATIENT_DOB --demo-col-person-race PATIENT_RACE and --demo-col-person-id --slurm-job-mem 5GB --slurm-job-time 4:00:00 --slurm-account myaccount --slurm-job-subset-start-index 1 --slurm-job-subset-end-index 10

The script creates in the slurm/job.output/ directory in the --lab-summary-dir directory and directs the slurm job output files to that directory.  The job array id is generated by the slurm system when the job array is submitted and shows up in the filename of the slurm job output files as follows:

--lab-summary-dir/slurm/job.output/substep_process_lab_(JOB ARRAY ID)_(LAB INDEX).out

You will need the job array id to run the step4_create_summary_grids_use_slurm_job_check.R script next.

II. After the jobs have run to completion, you need to check for jobs that failed using the Rscript step4_create_summary_grids_use_slurm_job_check.R script.  

This script requires the --lab-summary-dir parameter, which in most cases should be the same value you previously for step4_create_summary_grids_use_slurm.R.  

The script also takes the --slurm-job-mem and --slurm-job-time parameters, which are both required, and the optional --slurm-job-account parameter.  These should be set to the new values that hopefully will allow the failed jobs to run to completion.

Finally, the script takes the --slurm-job-array-id parameter.  This is the job array id number that was generated by the step4_create_summary_files_use_slurm.R.  You can find this number in the filename of the slurm job output files you can file in the slurm/job.output/ directory following the pattern --lab-summary-dir/slurm/job.output/substep_process_lab_(JOB ARRAY ID)_(LAB INDEX).out.

So if you want to rerun the failed jobs with, for example, 50GB of memory and 72 hours and a job array id of 1234567, you could run this script as follows:

Rscript step4_create_summary_grids_use_slurm_job_check.R --lab-summary-dir /path/to/QL/mylabsproject/labs_filt1_summaries/ --slurm-job-mem 50GB --slurm-job-time 72:00:00 --slurm-account myaccount --slurm-job-array-id 1234567

You need to rerun this script (with more job resources) until no missed jobs are found.  If more memory or time does not allow the jobs to run to completion you will need to manually examine those jobs' slurm output files and find the relevant error message which will hopefully guide you to a solution. :D

5.  Step 5 is the final step in the pipline.  It creates the summary matrices (tab-delimited) that has rows for all the PERSON_IDs and columns (like inverse medians) for all labs.  These are the files most useful for LabWAS analyses.

As with Step 4, you have the option to do this step with or without slurm jobs.  

5.a  To run this step without using slurm jobs, use the step5_create_summary_grids.R

This script has two required parameters:  
--lab-summary-dir, the path to the directory where the lab summary folders were generated.  This should generally be the same path used for this parameter in Step 4.   
--lab-summary-matrices-output-dir, the path to the directory where the lab summary matrices will be created.  This can be the same path as --lab-summary-dir.

Run this script as follows:

Rscript step5_create_summary_grids.R --lab-summary-dir /path/to/QL/mylabsproject/labs_filt1_summaries/ --lab-summary-matrices-output-dir /path/to/QL/mylabsproject/labs_filt1_summaries/summary_matrices/

There should be a set of three matrices generated by the script in /path/to/QL/mylabsproject/labs_filt1_summaries/summary_matrices/ if it runs to completion.

It's possible this step will exceed available memory limit, in which case you should try using the slurm-ready scripts which should avoid the memory problems.  

5.b  There are three substep scripts required to run this step with slurm: step5_create_summary_grids_use_slurm.R, step5_create_summary_grids_use_slurm_job_check.R, step5_create_summary_grids_create_final_matrices.R.

I.  The step5_create_summary_grids_use_slurm.R script will submit a slurm job array.  Each slurm job will generate the matrix rows for a group of PERSON_IDs.  This script has the same parameters as step5_create_summary_grids.R described in 5.a but with additional parameters: --num-person-per-batch and the slurm parameters --slurm-job-mem, --slurm-job-time. --slurm-job-account, --slurm-job-batch-start-index, --slurm-job-batch-end-index.  

The --num-person-per-batch is the numer of PERSON_IDs (matrix rows) that will be processed by each slurm job.  If not set it defaults to 1000 PERSON_IDs.  
The --slurm-job-mem and  --slurm-job-time parameters are both required.  Use full descriptions (like "5GB" and "24:00:00" not "5" and "24").  The optional --slurm-job-account parameter should be used if you have a particular linux user group that the jobs should be run with.

The --slurm-job-batch-start-index and --slurm-job-batch-end-index parameters allow you to specify a range of PERSON_ID batches to be submitted in the submitted slurm job array batch file.  This is useful if you want to conduct a test run with, say, --slurm-job-batch-start-index of 1 --slurm-job-batch-end-index 5, which would generate summary matrices for the first 5000 people (if you kept --num-person-per-batch to the default value of 1000).

If you were to run this script with 10GB for 1 day per batch of 5000 PERSON IDs, you probably run to run this script as follows:

Rscript step5_create_summary_grids_use_slurm.R --lab-summary-dir /path/to/QL/mylabsproject/labs_filt1_summaries/ --lab-summary-matrices-output-dir /path/to/QL/mylabsproject/labs_filt1_summaries_matrices/ --slurm-job-mem 10GB --slurm-job-time 24:00:00 --slurm-account myaccount --num-person-per-batch 5000


II.  After the jobs have run to completion, you need to check for jobs that failed using the Rscript step5_create_summary_grids_use_slurm_job_check.R script.  

This script requires the --lab-summary-matrices-output-dir parameter, which in most cases should be the same value you previously for step5_create_summary_grids_use_slurm.R.  

The script also takes the --slurm-job-mem, --slurm-job-time, which are both required, and the optional --slurm-job-account parameters.  These should be set to the new values that hopefully will allow the failed jobs to run to completion.

Finally, the script takes the --slurm-job-array-id parameter.  This is the job array id number that was generated by step5_create_summary_grids_use_slurm.R.  You can find this number in the filename of the slurm job output files you can file in the slurm/job.output/ directory following the pattern --lab-summary-dir/slurm/job.output/substep_process_lab_(JOB ARRAY ID)_(BATCH INDEX).out.

So if you want to rerun the failed jobs with, for example, 50GB of memory and 72 hours and a job array id of 1234567, you could run this script as follows:

Rscript step5_create_summary_grids_use_slurm_job_check.R --lab-summary-matrices-output-dir /path/to/QL/mylabsproject/labs_filt1_summaries_matrices/ --slurm-job-mem 50GB --slurm-job-time 72:00:00 --slurm-account myaccount --slurm-job-array-id 1234567

You need to rerun this script (with more job resources) until no missed jobs are found.  If more memory or time does not allow the jobs to run to completion you will need to manually examine those jobs' slurm output files and find the relevant error message which will hopefully guide you to a solution. :D

III.  After run substep II to completion (i.e. ALL of the PERSON_ID batch jobs have been completed), the last step is to run the step5_create_summary_grids_use_slurm_make_final_matrices.R script.  The script has one required parameter, --lab-summary-matrices-output-dir, which should be the same value you used from the previous substep.  This script will find the PERSONID_ row files generated by the slurm PERSON_ID batch jobs, located in the lab_update_indices/PERSON_ID_row_batches subdirectory found in the --lab-summary-matrices-output-dir directory, and concatenate them to generate the final matrices.  



